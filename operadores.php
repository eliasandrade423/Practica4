<html>
	<head>
		<title>Ejemplo de operadores logicos</title>
	</head>
	<body>
		<h1>Ejemplo de operadores logicos en PHP</h1>
		<?php
			$a = 8;
			$b = 3;
			$c = 3;
			echo ($a == $b) && ($c > $b), "<br>";
			echo ($a == $b) || ($b == $c), "<br>";
			echo !($b <= $c), "<br>";
			
			/*
				Anote el significado de los operadores logicos:
				1. && : (AND) Tienen que cumplirse todas las condiciones para que el resultado sea verdadero de lo contrario sera falso.
				2. || : (OR) Solo tiene que cumplirse una condicion para que que el resultado sea verdadero de lo contrario sera falso.			
			*/
			
			/*
				7. Resuelva las expresiones presentadas en el siguiente cuadro, escriba la respuesta (True or False):
				
				$i = 9;
				$f = 33.5;
				$c = ‘X’;
				
				1. ($i >= 6) && ($c == ‘X’) = TRUE
				2. ($i >= 6) || ($c == 12) = TRUE
				3. ($f < 11) && ($i > 100) = FALSE
				4. ($c != ‘P’) || (($i + $f) <= 10) = TRUE
				5. $i + $f <= 10 = FALSE
				6. $i >= 6 && $c == ‘X’ = TRUE
				7. $c != ‘P’ || $i + $f <= 10 = TRUE
				
			*/
		?>
	</body>
</html>