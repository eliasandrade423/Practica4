<html>
	<head>
		<title>Ejemplo de operaciones de comparacion</title>
	</head>
	<body>
		<h1>Ejemplo de operaciones de comparacion en PHP</h1>
		<?php
			$a = 8;
			$b = 3;
			$c = 3;
			echo $a == $b, "<br>";
			echo $a != $b, "<br>";
			echo $a < $b, "<br>";
			echo $a > $b, "<br>";
			echo $a >= $c, "<br>";
			echo $a <= $c, "<br>";
			
			/*
				Anote el significado de las operaciones de comparacion:
				
				1. == : Igual que
				2. != : Diferente de
				3. < : Menor que
				4. > : Mayor que
				5. >= : Mayor o igual que
				6. <= : Menor o igual que
			
			*/
		?>
	</body>
</html>